import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class SigChatLoadTest(unittest.TestCase):
    url_rp1 = "https://staging-sigchat.ivndev.com/QFQmiOV72"
    url_rp2 = "https://staging-sigchat.ivndev.com/4qZYSe5tt"

    driver_rp1 = webdriver.Firefox()
    driver_rp2 = webdriver.Firefox()

    message_1 = "I'm Here!"
    message_2 = "Is this real?"
    message_3 = "What are we going to do?"
    message_4 = "We are trying to load test right now!"
    # message_5 = "Meow meow meow meow meow meow meow meow meow meow MEOW!"
    message_6 = """
        Hey Bob, how are we doing on that Henderson file? The deadline is fast approaching and we really need to get this locked down. This is a Million Dollar Account and we don't want to let it slip through our fingers. We should meet for lunch on Wednesday, no make that Thursday, you know, to discuss this and see where we are. Let's meet at Carlito's Cabo Cactus Room for the Cactus Margaritas and we'll brainstorm the details. We don't want to let that jerk Bartowski get the account like this time last year.
        """

    messages = [
        message_1,
        message_2,
        message_3,
        message_4,
        # message_5,
        message_6
        ]

    def test_case_1(self):
        # RP1   
        driver_rp1 = self.driver_rp1
        driver_rp1.get(self.url_rp1)

        # assertion 1
        print("Asserting this is in fact Signal Chat...")
        self.assertIn("Signal Chat", driver_rp1.title)

        # pin input
        try:
            rp1_pin = WebDriverWait(driver_rp1, 10).until(
                EC.presence_of_element_located((By.TAG_NAME, "input"))
            )

        finally:
            rp1_pin = driver_rp1.find_element_by_tag_name("input")
            rp1_pin.send_keys("1234")
            time.sleep(1)

        # enter button
        try:
            rp1_button = WebDriverWait(driver_rp1, 10).until(
                EC.presence_of_element_located((By.TAG_NAME, "button"))
            )

        finally:
            rp1_button = driver_rp1.find_element_by_tag_name("button")
            rp1_button.click()
            time.sleep(1)

        print("RP1 in!")

        # RP2
        driver_rp2 = self.driver_rp2
        driver_rp2.get(self.url_rp2)

        # pin input
        try:
            rp2_pin = WebDriverWait(driver_rp2, 10).until(
                EC.presence_of_element_located((By.TAG_NAME, "input"))
            )

        finally:
            rp2_pin = driver_rp2.find_element_by_tag_name("input")
            rp2_pin.send_keys("1234")
            time.sleep(2)

        # enter button
        try:
            rp2_button = WebDriverWait(driver_rp2, 10).until(
                EC.presence_of_element_located((By.TAG_NAME, "button"))
            )

        finally:
            rp2_button = driver_rp2.find_element_by_tag_name("button")
            rp2_button.click()
            time.sleep(1)

        print("RP2 in!")

        # Chat time!
        for m in self.messages:
            try:
                rp2_chat = WebDriverWait(driver_rp2, 10).until(
                    EC.presence_of_element_located((By.TAG_NAME, "input"))
                )
                rp1_chat = WebDriverWait(driver_rp1, 10).until(
                    EC.presence_of_element_located((By.TAG_NAME, "input"))
                )

            finally:
                rp2_chat = driver_rp2.find_element_by_tag_name("input")
                rp1_chat = driver_rp1.find_element_by_tag_name("input")
                rp2_chat.send_keys(m)
                rp1_chat.send_keys(m)
                rp2_chat.send_keys(Keys.RETURN)
                rp1_chat.send_keys(Keys.RETURN)
                rp2_chat.clear()
                rp1_chat.clear()
                time.sleep(1)

        time.sleep(2)
        self.driver_rp1.close()
        self.driver_rp2.close()

        print("Finished")

if __name__ == "__main__":
    unittest.main()
